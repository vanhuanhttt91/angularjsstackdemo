﻿using AngularJSApp1._5.Bussiness;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.ServiceModel;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSApp1._5.ServiceInterface
{
    public class PostService : Service
    {
        public IPostBusiness _postBusiness { get; set; }

        public async Task<List<Post>> Get(Posts request)
        {
            var result = await _postBusiness.GetAllAsync().ConfigureAwait(false);
            return result.ConvertTo<List<Data.Post>>();
        }
    }
}
