﻿using AngularJSApp1._5.Bussiness;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.ServiceModel;
using ServiceStack;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace AngularJSApp1._5.ServiceInterface
{

    public class CategoriesService : Service
    {
        public ICategoriesBusiness _categoriesBusiness { get; set; }


        public async Task<List<Categories>> Get(GetAll request)
        {
            var result = await _categoriesBusiness.GetAllAsync().ConfigureAwait(false);
            return result.ConvertTo<List<Data.Categories>>();
        }

        public async Task<ResultData> Post(EditCategories request)
        {
            var result = new ResultData();
            var categories = request.ConvertTo<Categories>();
            var data = await _categoriesBusiness.EditAsync(categories).ConfigureAwait(false);
            result.IsError = !data;
            _ = data == true ? result.Message = "edit successfully!" : result.Message = "edit fail!";
            return result;
        }

        public async Task<ResultData> Post(AddCategories request)
        {
            var result = new ResultData();
            var categories = request.ConvertTo<Categories>();
            var data = await _categoriesBusiness.InsertAsync(categories).ConfigureAwait(false);
            result.IsError = !data;
            _ = data == true ? result.Message = "add successfully!" : result.Message = "add fail!";
            return result;
        }

        public async Task<ResultData> Post(DeleteCategories request)
        {
            var result = new ResultData();
            var data = await _categoriesBusiness.DeleteAsync(request.CategoriesId).ConfigureAwait(false);
            result.IsError = !data;
            _ = data == true ? result.Message = "delete successfully!" : result.Message = "delete fail!";
            return result;
        }
    }
}
