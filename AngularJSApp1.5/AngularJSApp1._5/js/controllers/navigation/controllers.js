﻿/* global angular */
(function () {
    "use strict";

    angular.module('navigation.controllers', [])
        .controller('navigationCtrl', ['$scope', '$location',
        function ($scope, $location) {
            $scope.IsRouteActive = function (routePath) {
                return routePath === $location.path();
            };
        }
    ]);
})();
