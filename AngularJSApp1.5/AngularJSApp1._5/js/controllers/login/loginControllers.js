﻿/* global angular */
(function () {
    "use strict";
    angular.module("login.controllers", ["kendo.directives"])
        .controller("loginCtrl", function ($scope) {
            $scope.data = [
                "12 Angry Men",
                "Il buono, il brutto, il cattivo.",
                "Inception",
                "One Flew Over the Cuckoo's Nest",
                "Pulp Fiction",
                "Schindler's List",
                "The Dark Knight",
                "The Godfather",
                "The Godfather: Part II",
                "The Shawshank Redemption"
            ];

            $scope.validate = function (event) {
                event.preventDefault();

                if ($scope.validator.validate()) {
                    $scope.validationMessage = "Hooray! Your tickets has been booked!";
                    $scope.validationClass = "valid";
                } else {
                    $scope.validationMessage = "Tên đăng nhập hoặc mật khẩu sai!";
                    $scope.validationClass = "invalid";
                }
            }
        })
})();
