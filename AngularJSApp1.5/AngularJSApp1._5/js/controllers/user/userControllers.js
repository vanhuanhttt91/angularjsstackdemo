﻿/* global angular */
(function () {
    "use strict";

    angular.module("user.controllers", ["kendo.directives"])
        .controller("userCtrl", function ($scope) {
            $scope.mainGridOptions = {
                dataSource: {
                    type: "odata",
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Employees"
                    },
                    pageSize: 5,
                    serverPaging: false,
                    serverSorting: false
                },
                sortable: true,
                pageable: true,
                editable: true,
                toolbar: ["create", "save", "cancel"],
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [{
                    field: "FirstName",
                    title: "First Name",
                    width: "120px"
                }, {
                    field: "LastName",
                    title: "Last Name",
                    width: "120px"
                }, {
                    field: "Country",
                    width: "120px"
                }, {
                    field: "City",
                    width: "120px"
                }, {
                    field: "Title"
                }]
            };

            $scope.detailGridOptions = function (dataItem) {
                return {
                    dataSource: {
                        type: "odata",
                        transport: {
                            read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                        },
                        serverPaging: false,
                        serverSorting: false,
                        serverFiltering: false,
                        pageSize: 5,
                        filter: { field: "EmployeeID", operator: "eq", value: dataItem.EmployeeID }
                    },
                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    editable: true,
                    toolbar: ["save", "cancel"],
                    columns: [
                        { field: "OrderID", title: "ID", width: "56px" },
                        { field: "ShipCountry", title: "Ship Country", width: "110px" },
                        { field: "ShipAddress", title: "Ship Address" },
                        { field: "ShipName", title: "Ship Name", width: "190px" }
                    ]
                };
            };
        })
})();
