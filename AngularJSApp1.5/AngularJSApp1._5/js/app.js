﻿/* global angular */
(function () {
    "use strict";
    // Declare app level module which depends on filters, and services
    var module = angular.module('app', [
        'ngRoute',
        'navigation.controllers',
        'user.controllers',
        'login.controllers',
        'register.controllers',
        'categories.controllers',
        'addCategories.controllers',
        'post.controllers'
    ]);

    //route view page
    module.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider.when('/', { templateUrl: '/partials/hello/hello.html', controller: 'helloCtrl' });
        $routeProvider.when('/view1', { templateUrl: '/partials/partial1.html' });
        $routeProvider.when('/view2', { templateUrl: '/partials/partial2.html' });
        $routeProvider.when('/404', { templateUrl: '/partials/404.html' });
        $routeProvider.otherwise({ redirectTo: '/404' });
        $routeProvider.when('/role', { templateUrl: '/views/role/role.html' });
        $routeProvider.when('/user', { templateUrl: '/views/user/user.html', controller: 'userCtrl' });
        $routeProvider.when('/login', { templateUrl: '/views/login/login.html', controller: 'loginCtrl' });
        $routeProvider.when('/register', { templateUrl: '/views/register/register.html', controller: 'registerCtrl' });
        $routeProvider.when('/categories', {
            templateUrl: '/views/categories/categories.html'
        });
        $routeProvider.when('/post', { templateUrl: '/views/post/post.html', controller: 'postCtrl' });
        $locationProvider.html5Mode(true);
    }]);

})();