﻿
function postServices($http) {
    return {
        addPost: function(item) {

            if (item === undefined || item === null) return;

            return $http({
                method: 'POST',
                params: item,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/addPost'
            }).then(getReponseComplete).catch(getReponseFail);
        },
        deletePost: function(item) {
            var request = {
                "PostId": item.PostId
            };
            return $http({
                method: 'POST',
                params: request,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/deletePost'
            }).then(getReponseComplete).catch(getReponseFail);
        },
        editPost: function(item) {
            if (item === undefined || item === null) {
                return;
            }
            return $http({
                method: 'POST',
                params: item,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/editPost'
            }).then(getReponseComplete).catch(getReponseFail);
        },
        readPost: function() {
            return $http({
                method: 'GET',
                params: {},
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/posts'
            }).then(getReponseComplete).catch(getReponseFail);
        }
    };
}