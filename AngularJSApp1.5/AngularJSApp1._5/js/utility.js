﻿var envBaseUrl = "http://localhost:55555/api";
var categoriesGlobal = [];

function ConvertJsonDateToDate(date) {
    if (date === null || date === undefined) return;
    var parsedDate = new Date(parseInt(date.substr(6)));
    var newDate = new Date(parsedDate);
    var month = ('0' + (newDate.getMonth() + 1)).slice(-2);
    var day = ('0' + newDate.getDate()).slice(-2);
    var year = newDate.getFullYear();
    return day + "/" + month + "/" + year;
}
function getReponseComplete(response) {
    return response.data;
}

function getReponseFail(response) {
    logger.error('XHR Failed for getAvengers.' + error.data);
    return response.data;
}

function notifyFailOptions() {
    return {
        templates: [{
            type: "error",
            template: $("#notificationFail").html()
        }]
    };
}
function notifySuccessOptions() {
    return {
        templates: [{
            type: "success",
            template: $("#notificationSuccess").html()
        }]
    };
}