﻿using System.IO;
using Funq;
using AngularJSApp1._5.ServiceInterface;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Razor;
using ServiceStack.OrmLite;
using System.Web.Configuration;
using ServiceStack.Data;
using AngularJSApp1._5.Repo.UnitOfWork;
using AngularJSApp1._5.Bussiness;
using ServiceStack.Auth;
using AngularJSApp1._5.Repo.EntityDto;

namespace AngularJSApp1._5
{
    public class AppHost : AppHostBase
    {
        /// <summary>
        /// Base constructor requires a Name and Assembly where web service implementation is located
        /// </summary>
        public AppHost()
            : base("AngularJSApp1._5", typeof(CategoriesService).Assembly)
        {
            var customSettings = new FileInfo(@"~/appsettings.txt".MapHostAbsolutePath());
            AppSettings = customSettings.Exists
                ? (IAppSettings)new TextFileSettings(customSettings.FullName)
                : new AppSettings();
        }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        public override void Configure(Container container)
        {
            //Config examples
            this.Plugins.Add(new PostmanFeature());
            this.Plugins.Add(new CorsFeature(allowedOrigins: "*",
                    allowedMethods: "GET, POST, PUT, DELETE, OPTIONS",
                    allowedHeaders: "Content-Type",
                    allowCredentials: false)
                );
            // this.Plugins.Add(new CorsFeature(allowedHeaders: "Content-Type, Authorization"));
            OrmLiteConfig.DialectProvider = SqlServerDialect.Provider;
            var connectionString = WebConfigurationManager.ConnectionStrings["ConnStringDb1"].ToString();
            container.Register<IDbConnectionFactory>(new OrmLiteConnectionFactory(connectionString));
            container.RegisterAutoWiredAs<UnitOfWork, IUnitOfWork>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<InitDatabase, IInitDataBase>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<CategoriesBusiness, ICategoriesBusiness>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<PostBusiness, IPostBusiness>().ReusedWithin(ReuseScope.Request);
            var initDb = container.Resolve<IInitDataBase>();
            initDb.Run();
            SettingAuthProvider(container);
            SetConfig(new HostConfig
            {
                DebugMode = AppSettings.Get("DebugMode", false),
                AddRedirectParamsToQueryString = true
            });

            this.Plugins.Add(new RazorFormat());
        }

        private void SettingAuthProvider(Container container)
        {
            var authRepo = new OrmLiteAuthRepository<CustomUserAuthEntity, CustomUserAuthDetailsEntity>(container.Resolve<IDbConnectionFactory>());
            container.Register<IUserAuthRepository>(authRepo);
            authRepo.InitSchema();

            Plugins.Add(new AuthFeature(() => new AuthUserSession(), new IAuthProvider[]
            {
                new JwtAuthProvider(AppSettings)
                {
                    RequireSecureConnection = false,
                    IncludeJwtInConvertSessionToTokenResponse = true,
                    ValidateToken = (o, request) => true
                },
                new CredentialsAuthProvider(AppSettings)
            })
            {
                IncludeRegistrationService = true,  // Allow register user
                AllowGetAuthenticateRequests = request => false,    // Require get authenticate info by other GET method
                DeleteSessionCookiesOnLogout = true

            });


            // Create admin account
            if (authRepo.GetUserAuthByUserName("admin") == null)
            {
                authRepo.CreateUserAuth(new CustomUserAuthEntity
                {
                    UserName = "admin",
                    Roles = { RoleNames.Admin },
                    DisplayName = "Admin"
                }, "pw");
            }
        }
    }
}