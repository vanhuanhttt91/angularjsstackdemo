﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using AngularJSApp1._5.Data;

namespace AngularJSApp1._5.ServiceModel
{
 
    public class CategoriesModel : IReturn
    {

    }

    [Route("/api/categories", "GET")]
    public class GetAll : IReturn<Categories>
    {

    }

    [Route("/api/editCategories", "POST")]
    public class EditCategories : Categories, IReturn<Categories>
    {
        
    }

    [Route("/api/addCategories", "POST")]
    public class AddCategories : Categories, IReturn<Categories>
    {

    }

    [Route("/api/deleteCategories", "POST")]
    public class DeleteCategories : IReturn
    {
        public int CategoriesId { get; set; }
    }
}
