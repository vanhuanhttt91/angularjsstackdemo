﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;
using AngularJSApp1._5.Repo.Repository;
using AngularJSApp1._5.Repo.UnitOfWork;

namespace AngularJSApp1._5.Bussiness
{
    public class PostBusiness : BaseBusiness, IPostBusiness
    {
        public IRepository<PostEntity> _repoPost;
        private readonly IUnitOfWork _unitOfWork;

        public PostBusiness(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _repoPost = unitOfWork.GetGenericRepository<PostEntity>();
            _unitOfWork = unitOfWork;
        }
        public Task<bool> DeleteAsync(int UserId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> EditAsync(PostEntity user)
        {
            throw new NotImplementedException();
        }
        public Task<bool> InsertAsync(PostEntity user)
        {

            throw new NotImplementedException();
        }

        public async Task<List<PostEntity>> GetAllAsync()
        {
            var result = await _repoPost.GetAllAsync();
            return result;
        }
    }

}
