﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;
using AngularJSApp1._5.Repo.Repository;
using AngularJSApp1._5.Repo.UnitOfWork;
using ServiceStack;

namespace AngularJSApp1._5.Bussiness
{
    public class CategoriesBusiness : BaseBusiness, ICategoriesBusiness
    {
        private readonly IRepository<CategoriesEntity> _categoriesRepo;
        private readonly IUnitOfWork _unitOfWork;
        public CategoriesBusiness(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _categoriesRepo = unitOfWork.GetGenericRepository<CategoriesEntity>();
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> DeleteAsync(int categoriesId)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                await _categoriesRepo.DeleteAsync(categoriesId);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditAsync(Categories categories)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                var categoriesEntity = categories.ConvertTo<CategoriesEntity>();
                categoriesEntity.EditDate = DateTime.UtcNow;

                await _categoriesRepo.UpdateAsync(categoriesEntity, fieldChanged => new { fieldChanged.CategoriesName, fieldChanged.EditDate },
                    condition => condition.CategoriesId == categories.CategoriesId);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<Categories>> GetAllAsync()
        {
            var categories = await _categoriesRepo.GetAllAsync();
            return categories.ConvertTo<List<Categories>>();
        }

        public async Task<List<Categories>> GetListAsync(CategoriesSearch request)
        {
            var query = _categoriesRepo.CreateQuery();
            var categories = await _categoriesRepo.GetAsync<CategoriesEntity>(query, request.Skip, request.Take, orderby: new string[] { nameof(Categories.CategoriesName) });
            return categories.ConvertTo<List<Categories>>();
        }

        public async Task<bool> InsertAsync(Categories categories)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                var categoriesEntity = categories.ConvertTo<CategoriesEntity>();
                categoriesEntity.CreateDate = DateTime.Now;
                await _categoriesRepo.InsertAsync(categoriesEntity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Task SearchAsync(Categories categories)
        {
            throw new NotImplementedException();
        }

        Task<List<Categories>> GetAsync()
        {
            throw new NotImplementedException();
        }
    }
}
