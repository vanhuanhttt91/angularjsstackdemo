﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;

namespace AngularJSApp1._5.Bussiness
{
    public interface ICategoriesBusiness
    {
        Task<bool> InsertAsync(Categories categories);
        Task<bool> EditAsync(Categories categories);
        Task<bool> DeleteAsync(int categoriesId);
        Task SearchAsync(Categories categories);
        Task<List<Categories>> GetListAsync(CategoriesSearch request);
        Task<List<Categories>> GetAllAsync();

    }
}
