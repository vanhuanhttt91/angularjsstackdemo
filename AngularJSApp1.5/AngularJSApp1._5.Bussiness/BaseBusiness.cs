﻿using AngularJSApp1._5.Repo.UnitOfWork;

namespace AngularJSApp1._5.Bussiness
{
    public class BaseBusiness
    {
        protected IUnitOfWork UnitOfWork;

        public BaseBusiness(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }
    }
}
