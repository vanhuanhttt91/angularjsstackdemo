﻿using AngularJSApp1._5.Repo.EntityDto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularJSApp1._5.Bussiness
{
    public interface IPostBusiness
    {
        Task<bool> InsertAsync(PostEntity user);
        Task<bool> DeleteAsync(int UserId);
        Task<bool> EditAsync(PostEntity user);
        Task<List<PostEntity>> GetAllAsync();

    }
}
