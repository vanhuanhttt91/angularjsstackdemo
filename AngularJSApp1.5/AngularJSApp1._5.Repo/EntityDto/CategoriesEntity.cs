﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AngularJSApp1._5.Repo.EntityDto
{
    [Alias("Categories")]
    public class CategoriesEntity
    {
        [AutoIncrement]
        [PrimaryKey]
        public int CategoriesId { get; set; }

        public string CategoriesName { get; set; }
        
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}
