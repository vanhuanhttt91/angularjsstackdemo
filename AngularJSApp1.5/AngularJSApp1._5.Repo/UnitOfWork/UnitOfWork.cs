﻿using AngularJSApp1._5.Repo.Repository;
using AngularJSApp1._5.Repo.UnitOfWork;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Data;

namespace AngularJSApp1._5.Repo.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        protected IDbConnection Connection;
        protected IDbTransaction Transaction;
        private bool _disposed;

        public UnitOfWork(IDbConnectionFactory dbConnectionFactory)
        {
            Connection = dbConnectionFactory.OpenDbConnection();
            Transaction = Connection.OpenTransaction();
        }

        public IRepository<T> GetGenericRepository<T>() where T : class
        {
            return new Repository<T>(Connection);
        }

        TRepository IUnitOfWork.GetRepository<TRepository>()
        {
            return (TRepository)Activator.CreateInstance(typeof(TRepository), Connection);
        }

        public void Commit()
        {
            try
            {
                Transaction.Commit();
            }
            catch
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                Transaction.Dispose();
            }
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (Transaction != null)
                    {
                        Transaction.Dispose();
                        Transaction = null;
                    }
                    if (Connection != null)
                    {
                        Connection.Dispose();
                        Connection = null;
                    }
                }
                _disposed = true;
            }
        }

        public void Rollback()
        {
            Transaction.Rollback();
        }
    }
}
