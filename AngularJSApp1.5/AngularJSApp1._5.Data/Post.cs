﻿
using System;

namespace AngularJSApp1._5.Data
{
    public class Post
    {
        public int PostId { get; set; }
        public string Slug { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public string CategoriesName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}
