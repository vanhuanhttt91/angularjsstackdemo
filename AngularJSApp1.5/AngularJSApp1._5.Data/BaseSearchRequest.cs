﻿using System.Runtime.Serialization;

namespace AngularJSApp1._5.Data
{
    public class BaseSearchRequest
    {
        [DataMember(Name = "$skip")]
        public int Skip { get; set; }

        [DataMember(Name = "$top")]
        public int Take { get; set; } = 10;

        [DataMember(Name = "$filter")]
        public string Filter { get; set; }
    }
}
