﻿
namespace AngularJSApp1._5.Data
{
    public class User
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public System.DateTime CreateDate { get; set; }

        public System.DateTime UpdateDate { get; set; }

        public int RoleId { get; set; }
    }
}
